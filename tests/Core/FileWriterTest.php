<?php

namespace App\tests\Core;

use App\Entity\File;
use PHPUnit\Framework\TestCase;
use src\Controler\FileWriter;


class FusionTest extends TestCase
{

    public function testPresenceAuMoins2Fichiers()
    {
        $files = \App\Controller\FileWriter::searchDir();
        $count = count($files);
        $this->assertGreaterThanOrEqual(2, $count);
    }

    public function testFormatFichier()
    {
        $files = \App\Controller\FileWriter::searchDir();
        $this->assertStringEndsWith('.csv', $files[0]);
        $this->assertStringEndsWith('.csv', $files[1]);
    }

    public function testPresenceDonneeTab1()
    {
        $tab = \App\Controller\FileWriter::readFrCsv();
        $count = count($tab);
        $this->assertGreaterThanOrEqual(1, $count);
    }

    public function testPresenceDonneeTab2()
    {
        $tab = \App\Controller\FileWriter::readGeCsv();
        $count = count($tab);
        $this->assertGreaterThanOrEqual(1, $count);
    }


    public function testCroiseeFrplusGrand(){
        $tabFr = \App\Controller\FileWriter::readFrCsv();
        $tabGlo = \App\Controller\FileWriter::readDlCsv();
        $tabGe = \App\Controller\FileWriter::readGeCsv();
        $this->assertEquals($tabGlo[0], $tabFr[0]);
        $this->assertEquals($tabGlo[1], $tabGe[0]);
        $this->assertEquals($tabGlo[2], $tabFr[1]);
        $this->assertEquals($tabGlo[3], $tabGe[1]);
        $this->assertEquals($tabGlo[4], $tabFr[2]);
        $this->assertEquals($tabGlo[5], $tabGe[2]);
        $this->assertEquals(end($tabGlo), end($tabFr));
    }

    public function testCroiseeGeplusGrandouEgal(){
        $tabGe = \App\Controller\FileWriter::readGeCsv();
        $tabFr = \App\Controller\FileWriter::readFrCsv();
        $tabGlo = \App\Controller\FileWriter::readDlCsv();
        $this->assertEquals($tabGlo[0], $tabFr[0]);
        $this->assertEquals($tabGlo[1], $tabGe[0]);
        $this->assertEquals($tabGlo[2], $tabFr[1]);
        $this->assertEquals($tabGlo[3], $tabGe[1]);
        $this->assertEquals($tabGlo[4], $tabFr[2]);
        $this->assertEquals($tabGlo[5], $tabGe[2]);
        $this->assertEquals(end($tabGlo), end($tabGe));
    }

    public function testSequentiel()
    {
        $tabFr = \App\Controller\FileWriter::readFrCsv();
        $tabGe = \App\Controller\FileWriter::readGeCsv();
        $tabGlo = \App\Controller\FileWriter::readDlCsv();
        $countGe = count($tabGe);
        $countFr = count($tabFr);
        $countGlo = count($tabGlo);
        $this->assertEquals($tabGlo[0], $tabFr[0]);
        $this->assertEquals($tabGlo[1], $tabFr[1]);
        $this->assertEquals($tabGlo[$countGlo - 2], $tabGe[$countGe - 2]);
        $this->assertEquals($tabGlo[$countGlo - 1], $tabGe[$countGe - 1]);
    }
}

?>