<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201016125651 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client ADD titre VARCHAR(255) NOT NULL, ADD nom VARCHAR(255) NOT NULL, ADD prenom VARCHAR(255) NOT NULL, ADD email VARCHAR(255) NOT NULL, ADD date VARCHAR(255) NOT NULL, ADD num_tel VARCHAR(255) NOT NULL, ADD cc_type VARCHAR(255) NOT NULL, ADD cc_number BIGINT NOT NULL, ADD cvv2 INT NOT NULL, ADD cc_expire VARCHAR(255) NOT NULL, ADD taille INT NOT NULL, ADD poids INT NOT NULL, ADD longitude VARCHAR(255) NOT NULL, ADD lattitude VARCHAR(255) NOT NULL, CHANGE name genre VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client ADD name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, DROP genre, DROP titre, DROP nom, DROP prenom, DROP email, DROP date, DROP num_tel, DROP cc_type, DROP cc_number, DROP cvv2, DROP cc_expire, DROP taille, DROP poids, DROP longitude, DROP lattitude');
    }
}
