<?php

namespace App\Controller;

use App\Entity\File;
use App\Form\UploadType;

use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class FileWriter extends AbstractController
{


    /**
     * @Route("/search",name="app_search")
     */
    public static function searchDir()
    {
        $dir = 'C:\projets-synfony\Sparkline\public\uploads';
        $files1 = scandir($dir);
        array_splice($files1, 0, 2);
        return $files1;
    }

    public static function readFrCsv()
    {
        $tabDir = FileWriter::searchDir();
        $filenameFrench = 'C:/projets-synfony/Sparkline/public/uploads/' . $tabDir[0];
        $FILE = fopen($filenameFrench, 'r');
        while ($frenchTab[] = fgetcsv($FILE, 2048, ",")) ;
        fclose($FILE);
        return $frenchTab;
    }

    public static function searchDirDl()
    {
        $dir = 'C:\projets-synfony\Sparkline\public\download/';
        $files1 = scandir($dir);
        array_splice($files1, 0, 2);
        return $files1;
    }

    public static function readDlCsv()
    {
        $tabDir = FileWriter::searchDirDl();
        $filename = 'C:\projets-synfony\Sparkline\public\download/' . $tabDir[0];
        $FILE = fopen($filename, 'r');
        while ($Tab[] = fgetcsv($FILE, 2048, ",")) ;
        fclose($FILE);
        return $Tab;
    }

    public static function readGeCsv()
    {
        $tabDir = FileWriter::searchDir();
        $filenameGerman = 'C:/projets-synfony/Sparkline/public/uploads/' . $tabDir[1];
        $FILE = fopen($filenameGerman, 'r');
        while ($germanTab[] = fgetcsv($FILE, 2048, ",")) ;
        fclose($FILE);
        array_splice($germanTab, 0, 1);
        return $germanTab;

    }

    /**
     * @Route("/fusion-seq",name="app_fusion_seq")
     */
    function writeCsv()
    {
        $frenchTab = FileWriter::readFrCsv();
        $germanTab = FileWriter::readGeCsv();
        $filenameGlo = 'download/french-german-data.csv';
        $FILE = fopen($filenameGlo, 'w');
        foreach ($frenchTab as $values) {
            if ($values) {
                fputcsv($FILE, $values, ",");
            }
        }
        foreach ($germanTab as $values) {
            if ($values) {
                fputcsv($FILE, $values, ",");
            }
        }
        fclose($FILE);
        return $this->redirectToRoute('app_dl');
    }

    /**
     * @Route("/fusion-cross",name="app_fusion_cross")
     */
    function writeCrossCsv()
    {
        $frenchTab = FileWriter::readFrCsv();
        $germanTab = FileWriter::readGeCsv();
        $filenameGlo = 'download/french-german-data.csv';
        $FILE = fopen($filenameGlo, 'w');
        $fr = count($frenchTab);
        $ge = count($germanTab);
        $x = 0;
        while ($x < $fr and $x < $ge) {
            if ($frenchTab[$x]) {
                fputcsv($FILE, $frenchTab[$x], ",");
            }
            if ($germanTab[$x]) {
                fputcsv($FILE, $germanTab[$x], ",");

            }
            $x++;
        }
        if ($fr < $ge) {
            $diff = $ge - $x;
            while ($x < $diff) {
                if ($germanTab[$x]) {
                    fputcsv($FILE, $germanTab[$x], ",");

                }
                $x++;
            }
        } elseif ($fr < $ge) {
            $diff = $fr - $x;
            while ($x < $diff) {
                if ($frenchTab[$x]) {
                    fputcsv($FILE, $frenchTab[$x], ",");

                }
                $x++;
            }
        }
        fclose($FILE);
        return $this->redirectToRoute('app_dl');
    }

    /**
     * @Route("/download-file",name="app_dl")
     */
    public function downloadCsv()
    {
        return $this->render('fileWriter/download.html.twig');
    }

    /**
     * @Route("/fusion",name="app_choice")
     */
    public function choiceFusion()
    {
        return $this->render('fileWriter/choice.html.twig');
    }




    /**
     * @Route("/file/injection", name="file_injection")
     */
    public function fileInjectionDb(EntityManagerInterface $em)
    {

        $tabDir = $this->searchDirDL();
        $csv_file = 'download/' . $tabDir[0];
        $FILE = fopen($csv_file, 'r');
        while ($csv_fileTab[] = fgetcsv($FILE, 1024, ",")) ;
        if (($handle = fopen($csv_file, "r")) !== FALSE) {
            fgetcsv($handle);
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                for ($c = 0; $c < $num; $c++) {
                    $col[$c] = $data[$c];
                }

                $gender = $col[1];
                $title = $col[3];
                $name = $col[4];
                $surname = $col[6];
                $date = $col[21];
                $num_tel = $col[18];
                $cc_type = $col[23];
                $cc_number = $col[24];
                $cvv2 = $col[25];
                $cc_expire = $col[26];
                $taille = $col[40];
                $poids = $col[38];
                $longitude = $col[41];
                $lattitude = $col[42];
                $email = $col[14];

                $brand = new Brand();
                $brand->setName('tesla');

                $vehicle = new Vehicule();
                $vehicle->setModele('model  x');
                $vehicle->setYear('2020');
                $vehicle->setBrand($brand);

                $client = new Client();
                $client->setGenre($gender);
                $client->setTitre($title);
                $client->setNom($surname);
                $client->setPrenom($name);
                $client->setDate($date);
                $client->setNumTel($num_tel);
                $client->getCCType($cc_type);
                $client->setCCNumber($cc_number);
                $client->setCvv2($cvv2);
                $client->setCcExpire($cc_expire);
                $client->setTaille($taille);
                $client->setPoids($poids);;
                $client->setLongitude($longitude);
                $client->setLattitude($lattitude);
                $client->setEmail($email);
                $client->setVehicule($vehicle);



                $em->persist($client);
                $em->persist($brand);
                $em->persist($vehicle);

            }
            fclose($handle);
            $em->flush();
        }
        return $this->redirectToRoute('app_homepage');
    }

}
