<?php

namespace App\Controller;

use App\Entity\File;
use App\Form\UploadType;

use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class FileInjection extends AbstractController
{

    /**
     * @Route("/",name="app_homepage")
     */
    public function homepage()
    {
        return $this->render('home/appIndex.html.twig');
    }


    /**
     * @Route("/upload", name="app_file_upload")
     */
    public function fileUpload(Request $request, FileUploader $fileUploader)
    {
        $upload = new File();
        $form = $this->createForm(UploadType::class, $upload);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $upload->getName();
            $fileName = $fileUploader->upload($file);
            $upload->setName($fileName);
            $this->addFlash('success', 'file uploaded successfully');
            return $this->redirectToRoute('app_homepage');
        }

        return $this->render('fileInjector/fileInjection.html.twig', array(
            'form' => $form->createView(),
        ));
    }


}