<?php
namespace App\Controller;

use App\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Client;
use App\Entity\Vehicule;
use App\Entity\Brand;
use Symfony\Component\Routing\Annotation\Route;


class ClientController extends AbstractController{


    /**
     * @Route("/client/{id}",name="client_list")
     */
    public function clientList(ClientRepository $clientRepository, $id)
    {
        $listClient = $clientRepository->find($id);

        $brand = $listClient->getVehicule()->getBrand()->getName();
        $vehiculeName = $listClient->getVehicule()->getModele();
        return $this->render('/client/clientList.html.twig', array('listClient'=>$listClient,
            'brand'=>$brand,
            'vehiculeName'=>$vehiculeName));
    }


    /**
     * @Route("/client/add", name="client_add_client")
     */
    public function addClient(EntityManagerInterface $em)
    {
        $brand = new Brand();
        $brand->setName('tesla');

        $vehicle = new Vehicule();
        $vehicle->setModele('model  x');
        $vehicle->setYear('2020');
        $vehicle->setBrand($brand);

        $client = new Client();
        $client->setName('george');
        $client->setVehicule($vehicle);

        $em->persist($brand);
        $em->persist($vehicle);
        $em->persist($client);
        $em->flush();

        return $this->redirectToRoute('client_list');
    }
}